//
//  SDSGraph.swift
//  SwiftDS
//
//  Created by Surendra Patel on 31/10/19.
//  Copyright © 2019 Surendra Patel. All rights reserved.
//

import Foundation

class GNode<T> where T: Equatable & Hashable {
    var destination: T
    var next: GNode<T>?
    
    init(with destination:T, and next: GNode<T>? = nil) {
        self.destination = destination
        self.next = next
    }
}

class NodeList<T> where T: Equatable & Hashable {
    var  listHead:GNode<T>
    
    init(with head:GNode<T>) {
        self.listHead = head
    }
}

public class Graph<T> where T: Equatable & Hashable {
    
    private var verticesNodes:[T:NodeList<T>]
    
    init() {
        self.verticesNodes = [:]
    }
    
    public func addEdge(from source:T,to destination:T) {
        
        if self.verticesNodes.keys.contains(source) {
            //Add to existing list
            let list = self.verticesNodes[source]
            let newNode = GNode(with: destination, and: list?.listHead)
            list?.listHead = newNode
        }
        else {
            //Add new list to vertices
            let nl:NodeList = NodeList<T>(with: GNode<T>(with: destination,and: nil))
            self.verticesNodes[source] = nl
        }
    }
    
    public func traverse() {
        
        for key:T in self.verticesNodes.keys {
            var list:GNode<T>? = self.verticesNodes[key]?.listHead
            var p:String = "Vertex \(key):"
            repeat {
                p.append(" > \(String(describing: list!.destination))")
                list = list!.next
            } while (list != nil)
            print(p)
        }
    }
    
    func DFS() {
        //TODO
    }
    
    func BFS() {
        //TODO
    }
}
