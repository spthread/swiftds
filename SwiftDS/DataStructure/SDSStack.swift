//
//  SDSStack.swift
//  SwiftDS
//
//  Created by Surendra Patel on 31/10/19.
//  Copyright © 2019 Surendra Patel. All rights reserved.
//

import Foundation

class Node<T> where T: Equatable & Comparable {
    var data:T
    var next:Node<T>?
    
    init(with inData:T, inNext:Node<T>? = nil) {
        self.data = inData
        self.next = inNext
    }
    
    deinit {
        print("deinit on - \(data)")
    }
}

public class Stack<T> : NSObject where T: Equatable & Comparable {
        
    var topNode:Node<T>?
    
    override init() {
        super.init()
    }
    
    init(with value:T) {
        super.init()
        self.push(value)
    }
    
    public func push(_ data:T) {
        let node = Node(with: data, inNext: nil)
        guard let _ = self.topNode else {
            self.topNode = node
            return
        }
        node.next = self.topNode
        self.topNode = node
    }
    
    public func pop() throws -> T? {
        guard let node = self.topNode else {
            throw DSError(description: "Stack is empty")
        }
        self.topNode = node.next
        return node.data
    }
    
    public func top() throws -> T? {
        guard let node = self.topNode else {
            throw DSError(description: "Stack is empty")
        }
        return node.data
    }
    
    public func traverse() throws -> Void {
        guard let _ = self.topNode else {
            throw DSError(description: "Stack is empty")
        }
        print("--------------Traverse--------------")
        var tempNode:Node? = self.topNode
        while tempNode != nil {
            print(tempNode?.data ?? "No Data")
            tempNode = tempNode?.next
        }
        print("--------------Traversed--------------")
    }
}
